# Copyright 2015 Steven Stewart-Gallus
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.  See the License for the specific language governing
# permissions and limitations under the License.

all: main

main.o: main.c

CFLAGS+=-g -Wall -Wextra -Wno-unused-parameter -Wno-missing-braces -Wno-missing-field-initializers -pthread -O3
LDADD+=-lm

main.c: \
  LntdConfig.nc\
  async.h\
  bool.h\
  error.h\
  ko.h\
  LntdAsyncCommand.nc\
  LntdAsyncIdle.nc\
  LntdAsyncPoller.nc\
  LntdAsyncReader.nc\
  LntdAsyncTimer.nc\
  LntdAsyncWriter.nc\
  LntdKoM.nc\
  LntdKo.nc\
  LntdLogger.nc\
  LntdMainLoop.nc\
  LntdNonblockPool.nc\
  LntdPoolIdle.nc\
  LntdPoolPoller.nc\
  LntdPoolReader.nc\
  LntdPoolStdio.nc\
  LntdPoolTimer.nc\
  LntdPoolWriter.nc\
  LntdSimulator.nc\
  LntdStartM.nc\
  LntdStart.nc\
  LntdStdioLogger.nc\
  LntdStdio.nc
	nescc -D_GNU_SOURCE=1 -conly -S -fnesc-cfile=$@ -Wnesc-all -Wnesc-error $<

main: main.o
	$(CC) -o $@ $(CFLAGS) $(LDFLAGS) $^ $(LDADD)
